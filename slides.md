---
author: Ioannis Papagiannidis, Tomislav Maric, Z-INF (CRC1194)
title: Singularity
date: June 2, 2020
---

# Intro to Singularity

## What is Singularity?
- A container platform
- Create and run containers

## Benefits
- Reproducible projects
- Have environment, code and data in one place
- No external dependencies

## How is it different from Docker?
- Focus on HPC clusters
- Portability: Single file easy to copy and share
- Open source
- Immutable containers
- No need to run as sudo. Retain privileges from host machine

## Library
- Container Libary
- Comperable to Docker Hub
~~~
singularity pull library library://sylabsed/linux/alpine
~~~

# Singualrity containers

## Creation methods
- Container Library
- Docker Hub
- existing container
- a Singularity Definition file

## The definition file
- Blueprints of custom container
- Two parts:
    1. Header
    2. Sections

## Header
- Describes the core operating system to build within the container.
- Top of the def file.
- **Bootstrap** indicates method
- **From** indicates name to look up
- Most common options for Bootstrap:
    1. library (Singularity Library)
    2. docker (Docker Hub)
    3. docker-daemon (running local images)
    4. localimage (Singularity container)

## Sections
- **%files**: Used to copy files from host into the container.
- **%post**: Used to configure the system (e.g. updates) at build time.
- **%runscript**: Script that is executed when container is run.
- **%environment**: Define environment variables. Available only at runtime, not build.
- **%test**: Runs after build and can be invoked afterwards.

## Example
~~~
Bootstrap: docker
From: archlinux

%files
    my_file /opt

%post
    pacman -Syyu
    git clone https://git.rwth-aachen.de/tester.git /opt/tester

%runscript
    echo "Hello world!"
~~~

## Building commands
~~~
sudo singularity build lolcow.sif library://sylabs-jms/testing/lolcow
~~~
~~~
sudo singularity build lolcow.sif docker://godlovedc/lolcow
~~~
~~~
sudo singularity build lolcow.sif lolcow.def
~~~

# Runscript for project lifecycle

## Common commands for scientific projects
- **clone**: get a copy of the project
- **build**: (re-)build the project
- **run**: (re-)execute simulation
- **test**: (re-)run tests on simulation results
- **jupyter**: Provide visual results to user

## Benefits
- Have a copy of the project and modify it
- Use the environment of the container
    - Without necessarely using the default project
- Interact as simple as possible with the container

## Structure
- Can use only bash language
    - Easier to inspect and modify
- or can write executable in another language and call it
    - May be easier to write in a higher level language

## Example
~~~
CMD=$1
SOURCE_DIR=`realpath "$2"`

case $CMD in
    clone)
        echo 'Copying project to $SOURCE_DIR.'
        cp -r /opt/lent $SOURCE_DIR
        ;;
    build)
        mkdir -p $SOURCE_DIR/build
        cd $SOURCE_DIR/build
        cmake -DCMAKE_INSTALL_PREFIX=./ ..
        make && make install
        ;;
esac
~~~

# Running the container

## Things to remember
- The current folder you initiate the command is mounted
- Also $HOME, /sys, /proc /tmp, /var/tmp, /etc/resolv.conf & passwd, and $PWD are mounted
- User's permission rights are retained inside the container
    - Unless you start the container with **sudo**

## shell
- Use shell to get inside the container interactively
~~~
singularity shell [-o overlay.img] my_image.sif
~~~

## run
- Use it in order to execute the **runscript** section
~~~
singularity run [-o overlay.img] my_image.sif clone
~~~
- Simpler form since the **runscript** will run by default
~~~
./my_image clone
~~~

## exec
- Use it when you want to by-pass the **runscript** and execute another command
~~~
singularity exec [-o overlay.img] my_image.sif /bin/bash -c "echo 'Hi'"
~~~

# Immutability

## Things to remember
- By default the container is **write-only**
    - Trying to create/modify/delete a file will result in an error
- In order to introduce mutability:
    - Overlay images
    - Create/Transform a sandbox container

## How to use overlay images as storage
- Creating an overlay image
~~~
dd if=/dev/zero of=overlay.img bs=1M count=500 && mkfs.ext3 overlay.img
~~~
- Use it when running the container interactivelly (shell)
~~~
singularity shell -o overlay.img my_image.sif
~~~
- Overlay image can later be resized

## Storage embedded inside the container
- Possible to embed the overlay image inside the container
- Can be removed afterwards
- Advantage: Only one file to worry about
~~~
singularity sif add [options...] my_image.sif overlay.img
~~~
- To start the container though **--writtable** option is needed
~~~
singularity shell --writable my_image.sif
~~~

## Creating a mutable container
- From the beginning create a container that is writtable
- Use the **--sandbox** option and the output file should in folder *format*
~~~
sudo singularity build --sandbox lolcow/ library://sylabs-jms/testing/lolcow
~~~
- Also turning an imuttable container into a muttable one
~~~
sudo singularity build development/ production.sif
~~~

# DEMO

## Project geophase
- Generic C++ library for geometrical operations on polygon / polyhedron soups
- C++ project with some other dependencies

## The Definition file (1)
- Specify the method & the image
~~~
Bootstrap: docker
From: archlinux:latest
~~~

## The Definition file (2)
- Get the fastest mirros
- Install updates and dependencies
~~~
%post
    pacman -Sy --noconfirm reflector
    reflector --latest 5 --sort rate --save /etc/pacman.d/mirrorlist
    pacman -Syu --noconfirm gcc cmake eigen python python-matplotlib texlive-core python-seaborn python-pandas jupyter make git png++ boost texlive-latexextra
    ...
~~~

## The Definition file (3)
- Clone the project and build it
- Use of folder outside of $HOME or any other mounted locations
~~~
...
    mkdir geophase
    git clone https://git.rwth-aachen.de/leia/geophase.git geophase/
    cd geophase/
    mkdir build && cd build/
    cmake -DCMAKE_BUILD_TYPE=Timing ..
    make
~~~

## The Definition file (4)
- Put logic inside the runscript functionality
- Our 'own' custom commands
~~~
%runscript

    # CMD : geophase image command (see help below).
    CMD=$1

    # Source directory
    SOURCE_DIR=$2

    # Build directory
    BUILD_DIR=$3

    case $CMD in
    ...
~~~

## The Definition file (5)
- *clone* will copy the project to **from** the container to to **host** system CWD
~~~
        clone) # Copy the 'geophase' to $SOURCE_DIR
            echo 'Copying the geophase project from the image to $SOURCE_DIR'
            cp -r /opt/geophase $SOURCE_DIR
            ;;
~~~

## The Definition file (6)
- *build* will build the local project
- **Overwritting** previous files
~~~
        build)
            # Build in $SOURCE_DIR/$BUILD_DIR
            rm -rf $SOURCE_DIR/$BUILD_DIR
            mkdir -p "$SOURCE_DIR/$BUILD_DIR"
            cd $SOURCE_DIR/$BUILD_DIR
            cmake -DCMAKE_BUILD_TYPE=Timing ..
            make
            ;;
~~~

## The Definition file (7)
- *re-run-tests* will run the tests again based on the new/old build
~~~
        re-run-tests)
            cp -r /opt/geophase/* $SOURCE_DIR
            cd $SOURCE_DIR/geophase/build
            ctest -V
            ;;
~~~

## The Definition file (8)
- *jupyter-notebook* will server the specified project's notebook on local machine
~~~
        jupyter-notebook)
            cd $SOURCE_DIR/jupyter-notebooks/
            jupyter notebook geophase-interface-positioning.ipynb --no-browser --port=8889
            ;;
    esac
~~~

## Using the container (1)
- Creates a copy of the project inside the current directory.
~~~
./geophase_container.sif clone
~~~

## Using the container (2)
- Builds the local copied project
- The project may be modified as long as no new dependencies are required
- The environment of the container is being used, since it contains all the required dependencies
~~~
./geophase_container.sif build geophase/
~~~

## Using the container (3)
- Executes the tests on the binaries built of the (modified) project
- Again the environment of the container is being used
~~~
./geophase_container.sif re-run-tests geophase/
~~~

## Using the container (4)
- Brings up a Jupyter server on the local host machine
- Serves the notebook of the local copied project 
~~~
./geophase_container.sif jupyter-notebook geophase/
~~~
